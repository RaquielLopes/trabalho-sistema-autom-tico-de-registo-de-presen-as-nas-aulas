package pt.uevora;

import org.junit.Before;
import org.junit.After;
import org.junit.Test;

import static org.junit.Assert.*;

public class PresencaTest{
    
    Presenca presenca;

    @Before
    public void setUp(){
        presenca = new Presenca();
    }

    @After
    public void down(){
        presenca = null;
    }

    @Test
    public void testpesquisa_hora() throws Exception{
        boolean result = presenca.pesquisa_hora(11,10,12);
        assertEquals("As 11 horas tem que estar entre as 10 e as 12",  true, (Object)result);
    }
    
    @Test
    
    public void testverificar_card() throws Exception{
        String result = presenca.verificar_card("Filip Cowan");
        assertEquals("O estudante Filip Cowan tem que possuir o cartao numero 006",  6D, (Object)result);
    }

    @Test
    public void testlevar_meia_presenca() throws Exception{
        boolean result = presenca.levar_meia_presenca(11,10,12);
        assertEquals("Se o aluno passa o cartao as 11 (Uma hora depois das 10 mas ainda em tempo de aula), deve levar meia presenca (true)",  true, (Object)result);
    }
}