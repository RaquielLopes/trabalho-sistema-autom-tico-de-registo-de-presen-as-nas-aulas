package pt.uevora;

import java.util.Scanner;

public class Presenca {
	static boolean pesquisa_hora(int hora_at , int hora_in, int hora_fim){
		if(hora_at >= hora_in && hora_at < hora_fim){ return true; } else { return false; }
	}
	 
	static boolean levar_meia_presenca(int hora_at , int hora_in, int hora_fim){
		if(hora_at >= hora_in+1 && hora_at <= hora_fim){ return true; } else { return false; }
	}

	static boolean verificar_card(String nomepessoa){
		return true;
	}
	
	
	public static void main(String args[]){
		boolean cartao_reconhecido = false;
		boolean aula_a_decorrer = false;
		boolean hora_atrasado = false;
		boolean dia_de_aula = false;
		boolean hora_manual = false;
		boolean dia_manual = false;
		String n_card ;
		int dia_atual=1;
		int mes_atual=6;
		int ano_atual=2020;
	    int hora_atual=11;
	    int minuto_atual=0;
	    int hora_inicio_aula=10;
	    int hora_fim_aula=12;
	    int k;
		Scanner input;
		input = new Scanner(System.in);
		String[] pessoas_presentes = new String[100];
		int ap = 0;
		int x = 0;
		int y = 0;
		boolean presente=false;
		String nome_util = "Nome";
		String papel = "Papel";
		int[] data_aula =     {2020,6,1,
		                  	   2020,6,8,
		                       2020,6,15,
		                       2020,6,22,
		                       2020,6,29,
		                       2020,7,1,
		                       2020,7,6,
		                       2020,7,13,
		                       2020,7,20,
		                       2020,7,27};
		String[] utilizadores =
		{"Hallam Burnett","001","aluno",
		"Lois Franks","002","aluno",
		"Miyah Kane","003","aluno",
		"Samanta Chaney","004","aluno",
		"Prisha Ball", "005", "aluno",
		"Filip Cowan", "006", "aluno",
		"Jarrad Fountain", "007", "aluno",
		"Janice Mccray", "008", "aluno",
		"Sebastien Moody", "009", "aluno",
		"Mia Mair", "010", "aluno",
		"Bentley Heaton","011", "aluno",
		"Ravinder Penn", "012", "aluno",
		"Prince Rich", "013", "aluno",
		"Aleeza Huynh", "014", "aluno",
		"Wesley Michael", "015", "aluno",
		"Fahima Carter", "016", "aluno",
		"Laura Walls", "017", "aluno",
		"Ophelia Baldwin", "018", "aluno",
		"Arnold Yu", "019", "aluno",
		"Bobby Whitaker", "020", "aluno",
		"Jorge Harper", "021", "aluno",
		"Mackenzie Emery", "022", "aluno",
		"Mohammod Dawe", "023", "aluno",
		"Nelly Waters", "024", "aluno",
		"Ricky Powers", "025", "aluno",
		"Denzel Reeve", "026", "aluno",
		"Bogdan Rosales", "027", "aluno",
		"Rehan Fisher", "028", "aluno",
		"Cordelia Swift", "029", "aluno",
		"Johan Mcpherson", "030", "aluno",
		"Everly Bailey", "031", "aluno",
		"Sabina English", "032", "aluno",
		"Brielle Whelan", "033", "aluno",
		"Gregor Santiago", "034", "aluno",
		"Dione Bowden", "035", "aluno",
		"Fiza Hewitt", "036", "aluno",
		"Shyam Person", "037", "aluno",
		"Mahima Roy", "038", "aluno",
		"Ralph Davidson", "039", "aluno",
		"Shanaya Battle", "040", "aluno",
		"Hasnain Patel", "041", "aluno",
		"Jonah Orr", "042", "aluno",
		"Dustin Lees", "043", "aluno",
		"Afsana Mora", "044", "aluno",
		"Brooklyn Haworth", "045", "aluno",
		"Kane Horn", "046", "aluno",
		"Esme Wiggins", "047", "aluno",
		"Leila Barnett", "048", "aluno",
		"Rahul Currie", "049", "aluno",
		"Wilfred Trevino", "050", "aluno",
		"Harvie Sharpe", "051", "aluno",
		"Alex Munro", "052", "aluno",
		"Aleyna Mcnally", "053", "aluno",
		"Frances Sheridan", "054", "aluno",
		"Alysha Dunlap", "055", "aluno",
		"Lilian Frye", "056", "aluno",
		"Sol Wang", "057", "aluno",
		"Blythe Frey", "058", "aluno",
		"Lukas Barr", "059", "aluno",
		"Rufus Odom", "060", "aluno",
		"Katharine Schroeder", "061", "aluno",
		"Maxime Lucero", "062", "aluno",
		"Aniela Snow", "063", "aluno",
		"Affan Cornish", "064", "aluno",
		"Cole Winters", "065", "aluno",
		"Sumayya Rollins", "066", "aluno",
		"Abbey Cherry", "067", "aluno",
		"Carrie-Ann O'Ryan", "068", "aluno",
		"Brandon-Lee Couch", "069", "aluno",
		"Poppy Francis", "070", "aluno",
		"Rea House", "071", "aluno",
		"Kareena Spears", "072", "aluno",
		"Malaikah Oconnor", "073", "aluno",
		"Musab Paine", "074", "aluno",
		"Tanvir Page", "075", "aluno",
		"Joey Strong", "076", "aluno",
		"Connie Mccormack", "077", "aluno",
		"Danniella Barton", "078", "aluno",
		"Sannah Whitfield", "079", "aluno",
		"Yaseen Kramer", "080", "aluno",
		"Oliver Portillo", "081", "aluno",
		"Korban Rossi", "082", "aluno",
		"Hussein Rhodes", "083", "aluno",
		"Veronica Singleton", "084", "aluno",
		"Zachery Grainger", "085", "aluno",
		"Gurdeep Tyson", "086", "aluno",
		"Zachariah Bowes", "087", "aluno",
		"Connagh Peralta", "088", "aluno",
		"Carlos Contreras", "089", "aluno",
		"Amelia-Mae Pittman", "090", "aluno",
		"Tamar Roberson", "091", "aluno",
		"Kaitlyn Greaves", "092", "aluno",
		"Peyton Riley", "093", "aluno",
		"Ronald Cruz", "094", "aluno",
		"Sahil Donald", "095", "aluno",
		"Austin Buckner", "096", "aluno",
		"Sarah-Louise Eaton", "097", "aluno",
		"Farrah Pham", "098", "aluno",
		"Efan Ibarra", "099", "aluno",
		"Leonie Leonard", "100", "docente"};
		

		while(true){		//Aqui começa o ciclo do programa
			if(hora_atual>0 && hora_atual<6){
				System.out.println("||Não há aulas, é de noite!|| "+"São "+ hora_atual + " horas e "+ minuto_atual+" minutos!");
				System.out.println(ano_atual+"/"+mes_atual+"/"+dia_atual);
			}
			if(hora_atual >= 6 && hora_atual <= 12){
				System.out.println("Bom dia!");
				System.out.println("São "+ hora_atual + " horas e "+ minuto_atual+" minutos!");
				System.out.println(ano_atual+"/"+mes_atual+"/"+dia_atual);
			}
			if(hora_atual>12 && hora_atual <= 20){
				System.out.println("Boa tarde!");
				System.out.println("São "+ hora_atual + " horas e "+ minuto_atual+" minutos!");
				System.out.println(ano_atual+"/"+mes_atual+"/"+dia_atual);
			}
			if(hora_atual>20 && hora_atual <= 23 || hora_atual == 0){
				System.out.println("Boa noite!");
				System.out.println("São "+ hora_atual + " horas e "+ minuto_atual+" minutos!");
				System.out.println(ano_atual+"/"+mes_atual+"/"+dia_atual);
			}
			System.out.println("");
			System.out.println("Por favor insira o número do cartão abaixo:");
			System.out.println("1000 Para consultar as presenças//2000 Para saltar uma hora//3000 Para saltar um dia//4000 Para terminar programa");
			System.out.printf("---»");												//1º Receber Input
			n_card = input.nextLine();
			if(n_card.contains("2000")){
				hora_manual=true;
				dia_manual=true;
				if(hora_atual==23){
					hora_atual=0;
					if(dia_atual==30){
						dia_atual = 0;
						mes_atual++;
					}
					dia_atual++;
				}
				hora_atual++;
			}
			if(n_card.contains("3000")){
				hora_manual=true;
				dia_manual=true;
				ap=0;
				for(int z=0;z<100;z++){
					pessoas_presentes[z]="";
				}
				if(dia_atual==30){
					dia_atual = 0;
					mes_atual++;
				}
				dia_atual++;
			}
			aula_a_decorrer = pesquisa_hora(hora_atual,hora_inicio_aula,hora_fim_aula);		//
			hora_atrasado = levar_meia_presenca(hora_atual,hora_inicio_aula,hora_fim_aula);	//2º Verificar hora atual
			for(x=1; x<299 ;x++){
				if(utilizadores[x].contains(n_card)){
					nome_util = utilizadores[x-1];
					papel = utilizadores[x+1];
					cartao_reconhecido = true;
					break;
				} else {
					cartao_reconhecido = false;
				}
			}
			for(k=2;k<30;k++){
				if(dia_atual==data_aula[k]){
					if(mes_atual==data_aula[k-1]){
						if(ano_atual==data_aula[k-2]){
							dia_de_aula=true;
							break;
						}
					}
				} else {
					dia_de_aula=false;
				}
			}
			if(ap>0){
				for(y=0; y<ap;y++){
					if(pessoas_presentes[y].contains(nome_util)){
						presente = true;
						break;
					}
					presente = false;
				}
			}
			if(n_card.contains("1000")){
				for(int p=0; p<ap ;p++){
					System.out.println(p+"-»"+pessoas_presentes[p]);
				}
				System.out.println(ap+" pessoas marcaram presença/meia presença.");
			}
			else if(n_card.contains("4000")){
				System.out.println("Programa Terminou!");
				break;
			} else {
				hora_manual=false;
				dia_manual=false;
				if(dia_de_aula){
					if(aula_a_decorrer){
						if(cartao_reconhecido){	
							if(presente){
								System.out.println("ERRO: Pessoa já tinha marcado presença antes nesta aula!");
							}else{
								if(hora_atrasado == false){
									pessoas_presentes[ap] = nome_util;
									ap++;
									System.out.println("Alerta: Foi marcada Presença com sucesso!"+"   Olá , "+papel+"/a"+" "+ nome_util+"! Boa aula.");
								} else {
									pessoas_presentes[ap] = nome_util;
									ap++;
									System.out.println("Alerta: Foi marcada Meia-Presença com sucesso!"+"   Olá , "+papel+"/a"+" "+ nome_util+"! Boa aula");
								}
							}
						}else{
							System.out.println("ERRO: Cartão não reconhecido!");
						}
					} else {
						System.out.println("ERRO: Não há aula a decorrer!");
					}
				} else {
					System.out.println("ERRO: Não é dia de aula!");
				}
			}
			if(minuto_atual>=45){
				if(hora_manual==false && dia_manual==false ){
					minuto_atual = 0;
				}
				hora_atual++;
				if(hora_atual >=24){
					ap=0;
					hora_atual=0;
					for(int z=0;z<100;z++){
						pessoas_presentes[z]="";
					}
					if(dia_atual==30){
						dia_atual=1;
						mes_atual++;
					}
					dia_atual++;
				}
			}else{
				if(hora_manual==false && dia_manual==false ){
					minuto_atual = minuto_atual + 15;
				}
			}
		}
	}
}