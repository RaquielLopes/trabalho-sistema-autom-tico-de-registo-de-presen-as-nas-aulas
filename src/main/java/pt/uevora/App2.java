package pt.uevora;
import java.util.Scanner;
public class App2 {
	public static int main(String args[]){
		while(true){
			System.out.println("|||||||||||*MENU PRINCIPAL*|||||||||||");
			System.out.println("|Escreva o nº da opção que desejar:  |");
			System.out.println("|____________________________________|");
			System.out.println("|(1)- Importar dados dos utilizadores|");
			System.out.println("|____________________________________|");
			System.out.println("|(2)- Justificar Falta               |");
			System.out.println("|____________________________________|");
			System.out.println("|(3)- Mostrar Relatório de Faltas    |");
			System.out.println("|____________________________________|");
			System.out.println("|(4)- Consultar faltas por aluno     |");
			System.out.println("|____________________________________|");
			System.out.println("||||||||||||||||||||||||||||||||||||||");
			System.out.printf("---» ");
			Scanner input = new Scanner(System.in);
			int num = input.nextInt();
			int t = 0;
			if(num == 1){
				//importar_dados_util();
				System.out.println("Não implementado, chamaria importar_dados_util() e importaria dados dos utilizadores");
				System.out.println("");
				t = 1;
				input.close();
				return t;
			} else if(num == 2) {
				System.out.printf("Insira o nº de aluno: ");
				int n_aluno= input.nextInt();
				System.out.println("Escreva a justificação: ");
				String just = input.next();
				System.out.println("Não implementado, chamaria justificar_falta(n_aluno,just) e a falta do aluno (n_aluno) seria justificada");
				t = 2;
				input.close();
				return t;
				// justificar_falta(n_aluno , just);  (Falta seria justificada)
			} else if(num == 3) {
				//mostrar_relatorio();
				System.out.println("Não implementado, chamaria mostrar_relatorio e mostraria o relatório se este existisse");
				System.out.println("");
				t = 3;
				input.close();
				return t;
			} else if(num == 4) {
				//consultar_faltas();
				System.out.println("Não implementado, chamaria consultar_faltas() e a tabela de faltas iria ser apresentada");
				System.out.println("");
				t = 4;
				input.close();
				return t;
			} else {
				t = 5;
				System.out.println("ERRO : OPÇÃO NÃO RECONHECIDA");
				input.close();
				return t;
            }     
        }
	}
}
