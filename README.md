# Sistema de registo de presenças nas aulas

 **Ano lectivo 2019/2020**
 				                   
**Metodologias e Desenvolvimento de Software**


**Docentes:** 

 * Pedro Salgueiro;
 * Pedro Patinho;

| Discentes | Número |
| ------ | ------ |
| Diogo Pinto | 43044 |
| Raquiel Lopes | 42075 |
						

**Objetivo:** O objetivo primordial da proposta do trabalho solicitado, é desenvolver sistema automático de registo de presenças nas aulas de MDS.

Na segunda parte do trabalho foi implementado o Sistema de Registo de Presenças, tendo como base os uses cases e o diagrama de classes desenvolvido na primeira parte do trabalho. Para tal, foi considerados os seguintes pontos:

		* Criar issues- foi Criado 3 issues,  que são: ->Marcação de presenças;->Relatório Semestral; e ->Alterar estado de faltas.
 		* Foi implementados testes unitários de forma a validar o caminho principal dos vários uses cases e também das extensões.

## 1º Especificação

Para a realização deste trabalho ,foram criados diagramas de use case, de classe, de sequência e de atividades, cada um deles com uma função especifica para nos ajudar no desenvolvimento das aplicações.
    

**Diagrama de use case**


- O diagrama de caso de uso descreve a funcionalidade proposta para um novo sistema que será projetado, é uma excelente ferramenta para o levantamento dos requisitos funcionais do sistema.


- Através deste diagrama é possivel representar as interações dos atores do sistema (Podem ser principais ou secundários), com o sistema.

**Diagrama de classe**


- É uma representação da estrutura e relações das classes que servem de modelo para objetos.É uma modelagem muito útil para o desenvolvimento de sistemas, pois define todas as classes que o sistema necessita possuir e é a base para a construção dos diagramas de comunicação, sequência e estados.Elemento abstrato que representa um conjunto de objetos. A classe contém a especificação do objeto; suas características: atributos (características) e métodos (ações / comportamentos).


- Foi usada para implementação da diagrama de classe do projeto(sistema automático de registo de presenças nas aulas) as seguinte classes:Pessoa, Aluno, Professor,Presença, Relatorio semestral e Aula.
E as seguintes associações: Aula e Relatório semestral,Aluno e Presença, entre outros.
    

**Diagrama de sequência**


- O diagrama de sequência é uma solução dinâmica de modelagem em UML bastante usada porque incide especificamente sobre linhas da vida, ou os processos e objetos que vivem simultaneamente, e as mensagens trocadas entre eles para desempenhar uma função antes do término da linha da vida.Faz a interação entre os objetos e determina a sequência de eventos que ocorrem em um determinado processo.

**Diagrama de Atividade**


- É utilizado para ilustrar uma sequência de atividades que foram modeladas para representar os aspectos dinâmicos de um processo computacional. Este modelo detalha o fluxo de controlo e o fluxo de dados de uma determinada atividade, mostrando as ramificações de controlo e as situações.
As sequência de atividades realizadas ao passar o cartão no leitor, são: passar o cartão,erro cartão não reconhecido, identificação da pessoa, Marcação da presença:-Marcação de meia presença ou Marcação de presença completa e envio de email ao aluno/professor.


## 2º Implementação


### App1 (Marcação de presenças)

- Esta aplicação funciona em modo terminal e tem como objetivo receber input textual (n_card) , identificar o utilizador e marcar presença/meiapresença a este (Caso esteja a decorrer aula);
- A aplicação foi implementada na classe publica Presenca, ocorrendo todo o fluxo do programa nesta.
	
**Análise do Código**
	
Foram utilizadas 4 funções:

->**boolean pesquisa_hora(int hora_at, int hora_in, int hora_fim)** -» Recebe como parâmetros a hora atual, a hora de inicio da aula e a hora de final da aula. Tem como objetivo determinar se a hora atual está dentro das horas de aula, caso esteja, significa que há aula a decorrer, retornando true. Caso o aluno/professor passe o cartão fora das horas de aula , a função retorna false.

->**boolean levar_meia_presenca(int hora_at, int hora_in, int hora_fim)** -» Recebe como parâmetros a hora atual, hora de inicio da aula e hora de final da aula. Tem como objetivo determinar se o aluno leva presença completa ou leva meia-presença, verificando se o aluno passa o cartão já uma hora depois da aula começar ou se passa ainda na primeira hora de aula. Se o aluno passar na primeira hora retorna false(presença). Caso contrário retorna true (meia-presença).

->**boolean verificar_card(String nomepessoa)** -» Recebe como parâmetro o nomedapessoa e vai verificar se o cartão inserido no input corresponde a esta pessoa ou não. Retorna true se corresponder , caso contrário retorna false.

->**public static void main(String args[])** -» Função onde ocorre todo o ciclo do programa, sendo esta a função principal.
                           
**1º** Ocorre declaração de variáveis essênciais ao funcionamento do programa;
	           
**2º** Arrays com datas das aulas e utilizadores são definidos;
	           
**3º** Ciclo while(true) inicia, parando apenas quando o input for 4000 (Código para o break;)

**INICIO DO CICLO DO SISTEMA**

**1º**-Ocorre verificação da hora atual, para decidir que altura do dia é e ocorre print da data atual e da hora atual.

**2º**-Utilizador passa o cartão(insere o n_card),podendo inserir 1000 para consultar as presenças, 2000 para adiantar uma hora , 3000 para adiantar um dia e 4000 para terminar o programa.
	    
**3º**- O programa vai verificar se o numero de cartão inserido é valido e vai verificar se é dia de aula.
        
**4º**-Sistema verifica se é hora de aula
	    
**5º**-Sistema verifica se a pessoa que passou o cartão já tinha marcado presença antes na mesma aula.

**6º**-Sistema vai percorrer uma serie de condições verificando o que fazer respetivamente a marcar presença (Pode marcar presença ou meia-presença.Podendo também não marcar nada).

**7º**-Relógio vai adiantar 15minutos e caso já estejamos no final do dia vai adiantar um dia.

**8º**-Repete-se o ciclo até comando 4000 ser inserido.

**FIM DO CICLO DO SISTEMA**

### App2 (Gestão do sistema)
	
-> Esta é uma aplicação simples que funciona em modo terminal, tem como objetivo imprimir um menu no ecrã com várias opções.
    (1-Importar dados dos utilizadores,2-Justificar Falta,3-Mostrar relatório de faltas,4-Consultar faltas por aluno)
		
-> O utilizador escolhe a opção que desejar e o programa posteriormente apresentaria o que foi pedido.	
(Mas nada mais foi implementado depois de se implementar o menu, pois apenas nos foi pedido para desenvolver um menu com estas opções, não as implementando internamente).
    
    
### Testing
		
-> Para testes utilizamos o JUnit, e implementamos os testes para a funcionalidade de marcar presenças.

-> Decidimos implementar testes para testar funções cruciais no funcionamento correto do sistema:

**testpesquisa_hora()** -»Verifica se o programa está a determinar corretamente se é hora de aula ou não (Caso haja problemas na função no programa principal, esta função vai lançar uma exceção.

**testverificar_card()** -» Vai verificar se a função verificar_card determina que a pessoa Filip Cowan tem o cartão 006 associado. Caso a função não saiba que o cartão 006 corresponde à pessoa Filip Cowan esta função vai lançar exceção.

**testlevar_meia_presenca()** -» Vai verificar se a função levar_meia_presença está a funcionar devidamente, ou seja, vai verificar se esta determina corretamente se o aluno deve levar meia presença ou não.
    
    
## Conclusão

A realização deste trabalho envolveu todos os temas lecionados nas aulas, é um acumular de todo o aprendizado do semestre num só trabalho, isto porque envolveu dominar-mos já com alguma facilidade 
os vários tipos de diagramas com as suas regras para os fazermos corretamente e foi também necessário já possuirmos algum entendimento sobre realização correta de software através da realização de
testes para testarmos aquilo que implementamos nas aplicações. Durante a implementação sentimos alguma facilidade relativamente à implementação das aplicações, pois já tinhamos diagramas feitos
que nos auxiliaram a tomar as decisões corretas , isto porque neles estava toda a estrutura do nosso sistema, ou seja, foi apenas necessário re-analisarmos os nossos diagramas para sabermos
como programar o nosso sistema corretamente.


Em jeito de conclusão, podemos dizer que este trabalho foi importantíssimo e foi uma grande ajuda na solidificação de grande parte do conhecimento obtido até agora no curso de Engenharia informática, isto porque
envolveu programarmos Java de forma correta, envolveu trabalharmos com git e com gitlab frequentemente (essencial para qualquer programador) , permitiu pôr em prática a criação de testes
usando JUnit e ajudou a solidificar muitos conceitos de lógica, tornando-nos de certa forma já mais eficientes ao desenvolver programas.
        